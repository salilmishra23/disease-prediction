# Disease Prediction on Apple Leaves

Demo classifier with fastai and Voila

To run the classifier, click on Binder logo.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/salilmishra23%2Fdisease-prediction/master?urlpath=%2Fvoila%2Frender%2Fleaf_classifier.ipynb)

